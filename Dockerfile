FROM golang:1.15-rc-alpine AS builder

WORKDIR /go/src/gitlab.com/Neverbolt/nativeinfinity
RUN apk add git alpine-sdk
RUN go get -u github.com/golang/dep/cmd/dep

COPY Gopkg.lock Gopkg.toml ./
RUN dep ensure --vendor-only

COPY main.go .

RUN GOOS=linux GOARCH=amd64 go build -o nativeinfinity .



FROM alpine:3.12
WORKDIR /root/
COPY --from=builder /go/src/gitlab.com/Neverbolt/nativeinfinity/nativeinfinity .

EXPOSE 4444
CMD ["./nativeinfinity"]