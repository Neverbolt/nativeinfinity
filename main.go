package main

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"math/rand"
	"net/http"
	"strings"
	"time"

	"github.com/google/uuid"
	"github.com/gorilla/mux"
)

const (
	// how many chunks will be served at maximum
	maxChunks = 300
	// how many pictures are there in a chunk
	imagesPerChunk = 7
	// how long do we allow between chunks to be loaded
	relTimeoutDuration = 100 * time.Second
	// how long is a connection allowed to live overall
	absTimeoutDuration = 3 * time.Minute
	// where do we serve from
	bind = ":4444"
)

const (
	// configuration for the generated background imagery
	// this low step count and high step size does not yield the most beautiful background, but it keeps generation
	// times low and is therefore able to be used (hopefully) without being hugged to death
	stepsX      = 10
	stepsY      = 10
	stepSize    = 40
	svgPreamble = "<svg xmlns=\"http://www.w3.org/2000/svg\" width=\"400\" height=\"400\" stroke=\"#000\" stroke-width=\"2\">"
	svgLine     = "<line x1=\"%d\" y1=\"%d\" x2=\"%d\" y2=\"%d\"/>"
	svgEpilog   = "</svg>"
)

var (
	connections = make(map[string]chan<- bool)
)

func main() {
	// initialize the router and server and serve our site!
	router := mux.NewRouter()
	router.HandleFunc("/", feed)
	router.HandleFunc("/image/{id:[a-zA-z\\-0-9]+}/{num:[0-9]+}.svg", serveImage)
	router.HandleFunc("/continue/{id:[a-zA-Z\\-0-9]+}/{num:[0-9]+}.svg", continueStream)

	logrus.Infof("Listening on %s", bind)
	logrus.Fatal(http.ListenAndServe(bind, router))
}

func feed(w http.ResponseWriter, r *http.Request) {
	// we need a flusher to push out the current chunk and bypass any buffering
	flusher, ok := w.(http.Flusher)
	if !ok {
		panic("expected http.ResponseWriter to be an http.Flusher")
	}

	// set up the headers, which is one of the key parts of this
	// "Keep-Alive" and "chunked" are used so that we can continually send out small parts of content without the
	// browser closing the connection on us
	w.Header().Set("Connection", "Keep-Alive")
	w.Header().Set("Transfer-Encoding", "chunked")
	// the content type and "nosniff" are set since there can be problems with chunked requests otherwise (allegedly)
	w.Header().Set("X-Content-Type-Options", "nosniff")
	w.Header().Set("Content-Type", "text/html")

	// this id is the identifier of the current request and is later on used to advance the chunks
	id := uuid.New().String()
	logrus.WithField("id", id).Infof("a new visitor has appeared")
	// we also need a random number generator per connection, since it is not thread-safe
	// random := rand.New(rand.NewSource(time.Now().UnixNano()))

	// this continuation channel is how the server knows to send another chunk to the client, it is used in
	// `continueStream` where given the request ID the correct channel is taken from the connections map and
	// a boolean signaling continuation is entered
	cont := make(chan bool, 1)
	connections[id] = cont

	// we send the preamble, which includes explanation text and a tiny bit of css
	err := sendPreamble(id, w, flusher)
	if err != nil {
		return
	}

	// then we send the first chunk, from which on the rest is done in the "main loop" below
	sendChunk(id, 0, w, flusher)

	// we need the chunk number to know which pictures have already been used, to reduce repetitions
	chunk := 1
	// start time is used for lazy-loading browser support detection
	start := time.Now()
	// the two timers are there to remove "dead" connections, relative is for between requests and absolute is for the
	// total age of a connection
	relativeTimeout := time.NewTimer(relTimeoutDuration)
	absoluteTimeout := time.NewTimer(absTimeoutDuration)

	loop:
	for {
		select {
		case <-cont: // a continuation has been requested
			// rate limiting, if the initial chunks have been loaded and the next chunks are loaded in extreme speeds
			// it is an indicator that the browser used does not support the lazy loading attribute and we therefore
			// terminate the connection
			if chunk > 5 && time.Now().Sub(start) / time.Duration(chunk) < 100 * time.Millisecond {
				logrus.WithField("id", id).Info("someone seems to be using an unsupported browser or is zoooooming")
				sendEnd("You appear to be using a browser that does not support lazy loading (or are scrolling waaaay faster that I thought anybody would). Please check if your browser supports the lazy loading property or reload and scroll a little more slowly so that rate limiting does not catch you ;)", w, flusher)
				break loop
			}

			relativeTimeout.Reset(relTimeoutDuration)
			sendChunk(id, chunk, w, flusher)

			// check if the maximum numbers of chunks has been sent, as at some point we want people to be gone from
			// our website and also a prompt to share at the very end is kinda cool
			chunk++
			if chunk > maxChunks {
				logrus.WithField("id", id).Infof("someone has reached the end after all %d chunks", maxChunks)
				sendEnd("You have reached the (artificially placed) end of this. If you enjoyed it be sure to share!", w, flusher)
				break loop
			}

		case <-relativeTimeout.C: // the relative timeout has been reached, quit the connection gracefully
			logrus.WithField("id", id).Infof("someone had a relative timeout after %d chunks", chunk)
			sendEnd(fmt.Sprintf("You have been artificially timed out, since your time between loading chunks was longer than %s, please reload the page to re-try.", relTimeoutDuration), w, flusher)
			delete(connections, id)
			break loop

		case <-absoluteTimeout.C: // the absolute timeout has been reached, quit the connection gracefully
			logrus.WithField("id", id).Infof("someone had an absolute timeout out after %d chunks", chunk)
			sendEnd(fmt.Sprintf("You have been artificially timed out, since your connection was older than %s, please reload the page to re-try.", absTimeoutDuration), w, flusher)
			delete(connections, id)
			break loop
		}
	}

	// since we don't have anything to do when the connection has been completed just move on
}

// continueStream is the endpoint that is called when a continuation should happen
func continueStream(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	// get the corresponding channel and if found activate the next chunk, otherwise just ignore it
	conn, ok := connections[vars["id"]]
	if ok {
		conn <- true
	}

	serveImage(w, r)
}

// this sends the introduction text as well as setup CSS
func sendPreamble(id string, w http.ResponseWriter, flusher http.Flusher) error {
	// it is not the prettiest having all this HTML inline, but this is a showcase script, to show the "cool" new
	// technology of javascript-less endless scrolling, so compromises were made to keep everything in one file
	_, err := fmt.Fprint(w, `<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Native infinite scroll</title>
		<style>
			body {
				margin: 0;
			}
			img {
				width: 25%;
				display: inline-block;
				margin: -2px;
			}
			.content {
				position: absolute;
				max-width: 600px;
				left: 0;
				right: 0;
				margin: 0 auto;
				margin-top: 1em;
				background: #FFFFFFF0;
				padding-left: 1.5em;
				padding-right: 1.5em;
				backdrop-filter: blur(2px);
			}
			.up {
				position: fixed;
				bottom: 3em;
				right: 3em;
				background: #FFFFFFF0;
				padding: 1em;
				backdrop-filter: blur(2px);
			}
			.up a {
				text-decoration: none;
			}
		</style>
	</head>
	<body id="top">
		<div class="content">
			<h1>Native infinite scroll</h1>
			<p>This website can do infinite scrolling without javascript (javascript needs to be enabled though, as browsers can disable lazy loading of images when javascript is disabled, to disallow scroll tracking).</p>
			<p>It uses the chromium lazy loading attribute for images with <pre>&lt;img&nbsp;src="/continue/{connection-id}/{chunk-number}.svg"&nbsp;loading="lazy"&gt;</pre> so you might need to enable that in <a href="chrome://flags/#enable-lazy-loading">chrome://flags/#enable-lazy-loading</a> (chrome may not allow clicking this link, just copy and paste it).</p>
			<p>I know that there should be a firefox feature that does the same, however, I have not been able to find it in my <a href="about:config">about:config</a>, maybe you are more successful.</p>
			<p>The backend is a go server that transmits in chunked HTTP and releases another chunk as soon as the last chunk image is tried to be lazily loaded. It also "procedurally" generates some nice background visuals for your viewing pleasure</p>
			<p>You can find the source on my <a href="https://gitlab.com/Neverbolt/nativeinfinity">Gitlab</a></p>
			<h2>Scroll as you please!</h2>
		</div>
		<div class="up"><a href="#top">up</a></div>
`)
	if err != nil {
		return err
	}
	flusher.Flush()

	return nil
}

// send chunk continues a current connection and sends another chunk of image tags
func sendChunk(id string, chunk int, w http.ResponseWriter, flusher http.Flusher) {
	content := ""
	// if we are at the 0th chunk then start the "misalignment"
	// it basically means, that in the last row the last tile is always free, which makes any message we put at the end
	// better looking, since it always completes the lin
	i := 0
	if chunk == 0 {
		i = 1
	}

	// pump out all images followed by our continuation image
	for ; i < imagesPerChunk; i++ {
		content += fmt.Sprintf(`		<img src="/image/%s/%d.svg" width='25%%' height='25%%' intrinsic-size='400x400'>`, id, chunk*imagesPerChunk+i)
	}
	content += fmt.Sprintf("		<img loading='lazy' src='/continue/%s/%d.svg' width='25%%' height='25%%' intrinsic-size='400x400'>\n", id, chunk)

	_, err := fmt.Fprint(w, content)
	if err != nil {
		logrus.WithError(err).Errorf("Could not properly send content")
	}
	flusher.Flush()
}

// for which ever reason we have reached an end and say goodbye to our visitor
func sendEnd(reason string, w http.ResponseWriter, flusher http.Flusher) {
	content := fmt.Sprintf("		<div style='width: 25%%; display: inline-block;'>%s</div>\n	</body>\n</html>\n\r", reason)

	_, err := fmt.Fprint(w, content)
	if err != nil {
		logrus.WithError(err).Errorf("Could not properly send content")
	}

	flusher.Flush()
}

// serveImage takes a generated SVG and serves it as image file
func serveImage(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "image/svg+xml")

	image, err := generateImage()
	if err != nil {
		logrus.WithError(err).Errorf("Could not generate image")
	}

	_, err = w.Write(image)
	if err != nil {
		logrus.WithError(err).Errorf("Could not send image")
	}
}

func generateImage() ([]byte, error) {
	random := rand.New(rand.NewSource(time.Now().UnixNano()))
	var err error

	// the content is where we build the generated image in
	// we pre-grow it, to allow it to contain the necessary lines (hopefully) without needing to resize it
	var content strings.Builder
	content.Grow(len(svgPreamble) + stepsX*stepsY*(len(svgLine)+4*3) + len(svgEpilog))

	_, err = content.WriteString(svgPreamble)
	if err != nil {
		return nil, err
	}

	// write the actual content, inspired by https://generativeartistry.com/tutorials/tiled-lines/
	for x := 0; x < stepsX; x++ {
		for y := 0; y < stepsY; y++ {
			if random.Int() % 2 == 0 {
				_, err = content.WriteString(fmt.Sprintf(svgLine, x*stepSize, y*stepSize, (x+1)*stepSize, (y+1)*stepSize))
			} else {
				_, err = content.WriteString(fmt.Sprintf(svgLine, (x+1)*stepSize, y*stepSize, x*stepSize, (y+1)*stepSize))
			}
			if err != nil {
				return nil, err
			}
		}
	}

	_, err = content.WriteString(svgEpilog)
	if err != nil {
		return nil, err
	}

	return []byte(content.String()), nil
}
