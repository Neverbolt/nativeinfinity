# Native Infinity Scrolling

This is a demo of the possibility to now build infinitely scrolling websites without a single line of javascript. You can (hopefully) find a live version over on [infinity.localhost.jetzt](https://infinity.localhost.jetzt).

It is build upon the "newly" integrated feature of lazy image loading, which is available in Chrome (and maybe also Firefox).

The code kept in one [file](main.go) only on purpose, as it was semi-quickly built up as hack and since it allows for easy showcasing of all components.

The docker container described in the [Dockerfile](Dockerfile) can be used to fully build the server and serve it on port 4444.